from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
# from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import *
from django.urls import reverse_lazy
from Soutenances.models import *
from .serializers import *
from GestionSoutenances.settings import *

from rest_framework import generics

# def home(request):
#     slots = Slots.objects.all()
#     return render(request, "Soutenances/calendar2.html", {
#         "slots": slots
#     })

def register_choice(request): #pour choisir entre inscription de professeur et d'étudiant
    return render(request, "Soutenances/register_choice.html")

def register_student(request):
    groupes = Groupe.objects.all()
    return render(request, "Soutenances/studentregister.html", {
        "groupes": groupes
    })

def register_teacher(request):
    return render(request, "Soutenances/teacherregister.html")

def inscription_student(request):
    nom = request.POST['nom']
    prenom = request.POST['prenom']
    pseudo = request.POST['pseudo']
    password = request.POST['password']
    classe = request.POST['classe']
    User.objects.create(nom=nom, prenom=prenom, pseudo=pseudo, password=password, classe=classe, groupe="eleves")
    user = User.objects.get(pseudo=pseudo, password=password) # récupère le compte créé
    groupe = Groupe.objects.get(nom=classe) #récupération de l'instance de groupe
    Etudiant.objects.create(nom=nom, prenom=prenom, groupe=groupe, user=user) #création de l'étudiant
    return render(request, "Soutenances/connexion.html")

def inscription_teacher(request):
    nom = request.POST['nom']
    prenom = request.POST['prenom']
    pseudo = request.POST['pseudo']
    password = request.POST['password']
    User.objects.create(nom=nom, prenom=prenom, pseudo=pseudo, password=password, groupe="professeurs")
    user = User.objects.get(pseudo=pseudo, password=password) # récupère le compte créé
    Enseignant.objects.create(nom=nom, prenom=prenom, user=user)
    return render(request, "Soutenances/connexion.html")

def login_form(request):
    return render(request, "Soutenances/connexion.html")

def mesSoutenances(request):
    user = request.user
    if user.groupe == "eleves":
        etudiant = Etudiant.objects.get(user=user)
        groupeSoutenance = GroupeSoutenance.objects.all()
        for groupe in groupeSoutenance:
            if etudiant in groupe.etudiants.all():
                soutenances = Soutenance.objects.filter(groupeSoutenance=groupe)
        return render(request, "Soutenances/mesSoutenances.html", {
            "user": user,
            "etudiant": etudiant,
            "soutenances": soutenances
        })
    elif user.groupe == "professeurs":
        enseignant = Enseignant.objects.get(user=user)
        groupeSoutenance = GroupeSoutenance.objects.all()
        for groupe in groupeSoutenance:
            if enseignant == groupe.evenement.enseignant:
                soutenances = Soutenance.objects.filter(groupeSoutenance=groupe)
        return render(request, "Soutenances/mesSoutenances.html", {
            "user": user,
            "enseignant": enseignant,
            "soutenances": soutenances
        })

class CustomAuth(object):

    def authenticate(self, nom=None, password=None):
        try:
            user = User.objects.get(pseudo=nom)
            if user.password == password:
                return user
        except:
            user = None
            return user

    def get_user(self, nom):
        user = User.objects.get(pseudo=nom)
        if user.is_active:
            return user
        return None

def connexion(request):
    username = request.POST['username']
    password = request.POST['password']
    auth = CustomAuth()
    user = auth.authenticate(nom=username, password=password)
    if user is not None:
        login(request, user)
        return render(request, "Soutenances/accueil.html", {
            "user": user
        })
    else:
        return render(request, "Soutenances/connexion.html", {
            "error": "La combinaison nom / mot de passe ne correspond pas"
        })

def deconnexion(request):
    logout(request)
    return render(request, "Soutenances/accueil.html", {
        "user": None
    })

def profil(request):
    user = request.user # récupère l'utilisateur connecté
    if user.groupe == "eleves":
        etudiant = Etudiant.objects.get(user=user) #récupération de l'étudiant associé
        groupes = Groupe.objects.all()
        return render(request, "Soutenances/profil.html", {
            "user": user,
            "etudiant": etudiant,
            "groupes": groupes
        })
    elif user.groupe == "professeurs":
        enseignant = Enseignant.objects.get(user=user)
        return render(request, "Soutenances/profil.html", {
            "user": user,
            "enseignant": enseignant
        })
    return None

def updateprofil(request):
    user = request.user # récupère l'utilisateur connecté
    if user.groupe == "eleves":
        etudiant = Etudiant.objects.get(user=user)
        nom = request.POST['nom']
        prenom = request.POST['prenom']
        detail = request.POST['detail']
        try:
            photo = request.FILES['photo']
            etudiant.photo = photo
        except:
            print("doesn't work")
        groupe = Groupe.objects.get(nom=request.POST['groupe'])

        etudiant.nom = nom
        etudiant.prenom = prenom
        etudiant.detail = detail
        etudiant.groupe = groupe
        etudiant.save()

        user.nom = nom
        user.prenom = prenom
        user.classe = groupe.nom
        user.save()

        groupes = Groupe.objects.all()
        return render(request, "Soutenances/profil.html", {
            "user": user,
            "etudiant": etudiant,
            "groupes": groupes
        })
    elif user.groupe == "professeurs":
        enseignant = Enseignant.objects.get(user=user)
        nom = request.POST['nom']
        prenom = request.POST['prenom']
        detail = request.POST['detail']
        try:
            photo = request.FILES['photo']
            enseignant.photo = photo
        except:
            print("doesn't work")

        enseignant.nom = nom
        enseignant.prenom = prenom
        enseignant.detail = detail
        enseignant.save()

        user.nom = nom
        user.prenom = prenom
        user.save()

        return render(request, "Soutenances/profil.html", {
            "user": user,
            "enseignant": enseignant
        })

# Gestion des groupes

class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class GroupeList(generics.ListCreateAPIView):
    queryset = Groupe.objects.all()
    serializer_class = GroupeSerializer

class GroupeDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Groupe.objects.all()
    serializer_class = GroupeSerializer

class EtudiantList(generics.ListCreateAPIView):
    queryset = Etudiant.objects.all()
    serializer_class = EtudiantSerializer

class EtudiantDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Etudiant.objects.all()
    serializer_class = EtudiantSerializer


class EnseignantList(generics.ListCreateAPIView):
    queryset = Enseignant.objects.all()
    serializer_class = EnseignantSerializer

class EnseignantDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Enseignant.objects.all()
    serializer_class = EnseignantSerializer


class EvenementList(generics.ListCreateAPIView):
    queryset = Groupe.objects.all()
    serializer_class = GroupeSerializer

class EvenementDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Evenement.objects.all()
    serializer_class = EvenementSerializer

class GroupeSoutenanceList(generics.ListCreateAPIView):
    queryset = GroupeSoutenance.objects.all()
    serializer_class = GroupeSoutenanceSerializer

class GroupeSoutenanceDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = GroupeSoutenance.objects.all()
    serializer_class = GroupeSoutenanceSerializer

class SlotsList(generics.ListCreateAPIView):
    queryset = Slots.objects.all()
    serializer_class = SlotsSerializer

class SlotsDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Slots.objects.all()
    serializer_class = SlotsSerializer

class SoutenanceList(generics.ListCreateAPIView):
    queryset = Soutenance.objects.all()
    serializer_class = SoutenanceSerializer

class SoutenanceDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Soutenance.objects.all()
    serializer_class = SoutenanceSerializer

from django import forms

class GroupeForm(forms.ModelForm):
    class Meta:
        model = Groupe
        fields = ['nom']
        widgets = {
            'nom': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nom'}),
        }

class GroupeCreateView(CreateView):
    '''
    Ajout d'un groupe
    '''
    model = Groupe
    # fields = ['nom']
    form_class = GroupeForm

class ListeGroupeView(ListView):
    '''
    Liste des groupes
    '''
    model = Groupe
    def get_context_data(self, **kwargs):
        context = super(ListeGroupeView, self).get_context_data(**kwargs)
        context["groupes"] = Groupe.objects.all()
        return context

class GroupeUpdate(UpdateView):
    """
    Modification d'un groupe
    """
    model = Groupe
    # fields = ['nom']
    template_name_suffix = '_update_form'
    form_class = GroupeForm


class GroupeDelete(DeleteView):
    model = Groupe
    success_url = "/soutenances/groupes"

# Gestion des étudiants

class EtudiantForm(forms.ModelForm):
    class Meta:
        model = Etudiant
        fields = ['nom', 'prenom', 'photo', 'detail', 'groupe']
        widgets = {
            'nom': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nom'}),
            'prenom': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Prénom'}),
            'photo': forms.FileInput(attrs={'class': 'form-control-file', "title": "Sélectionnez votre image de profil"}),
            'detail': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Detail'}),
            'groupe': forms.Select(attrs={'class': 'form-control', "title": "Sélectionnez votre groupe"}),
        }

class EtudiantCreateView(CreateView):
    '''
    Ajout d'un etudiant
    '''
    model = Etudiant
    # fields = ['nom', 'prenom', 'photo', 'detail', 'groupe']
    form_class = EtudiantForm

class ListeEtudiantView(ListView):
    """
    Liste des étudiants
    """
    model = Etudiant

    def get_context_data(self, **kwargs):
        context = super(ListeEtudiantView, self).get_context_data(**kwargs)
        context["etudiants"] = Etudiant.objects.all()
        return context

class EtudiantUpdate(UpdateView):
    '''
    Modification d'un etudiant
    '''
    model = Etudiant
    # fields = ['nom', 'prenom', 'detail', 'groupe']
    template_name_suffix = '_update_form'
    form_class = EtudiantForm

class EtudiantDelete(DeleteView):
    model = Etudiant
    success_url = "/soutenances/etudiants"

# Gestion des Enseignants

class EnseignantForm(forms.ModelForm):
    class Meta:
        model = Enseignant
        fields = ['nom', 'prenom', 'photo', 'detail']
        widgets = {
            'nom': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nom'}),
            'prenom': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Prénom'}),
            'photo': forms.FileInput(attrs={'class': 'form-control-file', "title": "Sélectionnez votre image de profil"}),
            'detail': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Detail'}),
        }

class EnseignantCreateView(CreateView):
    '''
     ajout d'un groupe
    '''
    model = Enseignant
    # fields = ['nom', 'prenom', 'photo', 'detail']
    form_class = EnseignantForm


class ListeEnseignantView(ListView):
    '''
    liste des enseignant
    '''
    model = Enseignant

    def get_context_data(self, **kwargs):
        context = super(ListeEnseignantView, self).get_context_data(**kwargs)
        context["enseignants"] = Enseignant.objects.all()
        return context

class EnseignantUpdate(UpdateView):
    '''
    Modification d'un enseignant
    '''
    model = Enseignant
    # fields = ['nom', 'prenom', 'detail']
    template_name_suffix = '_update_form'
    form_class = EnseignantForm

class EnseignantDelete(DeleteView):
    model = Enseignant
    success_url = "/soutenances/enseignants"

# Gestion des Evénements

class EvenementForm(forms.ModelForm):
    class Meta:
        model = Evenement
        fields = ['nom', 'date_debut', 'date_fin', 'dureeSoutenances', 'enseignant','groupe']
        widgets = {
            'nom': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nom'}),
            'date_debut': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Date de début'}),
            'date_fin': forms.TextInput(attrs={'class': 'form-control', "placeholder": "Date de fin"}),
            'dureeSoutenances': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Durée de soutenance'}),
            'enseignant': forms.Select(attrs={'class': 'form-control', "title": "Sélectionnez l'enseignant responsable"}),
            'groupe': forms.Select(attrs={'class': 'form-control', "title": "Sélectionnez le groupe concerné"}),
        }

class EvenementCreateView(CreateView):
    '''
     ajout d'un événement
    '''
    model = Evenement
    # fields = ['nom', 'date_debut', 'date_fin', 'dureeSoutenances', 'enseignant','groupe']
    form_class = EvenementForm

class ListeEvenementView(ListView):
    '''
    liste des événements
    '''
    model = Evenement

    def get_context_data(self, **kwargs):
        context = super(ListeEvenementView, self).get_context_data(**kwargs)
        context["evenements"] = Evenement.objects.all()
        return context

class EvenementUpdate(UpdateView):
    '''
    Modification d'un événement
    '''
    model = Evenement
    # fields = ['nom', 'date_debut', 'date_fin', 'dureeSoutenances', 'enseignant','groupe']
    template_name_suffix = '_update_form'
    form_class = EvenementForm

class EvenementDelete(DeleteView):
    model = Evenement
    success_url = "/soutenances/evenements"


class EvenementDetail(DetailView):
    model= Evenement


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)


        return context


class GroupeSoutenanceCreateView(CreateView):
    '''
     ajout d'un groupe
    '''
    model = GroupeSoutenance
    fields = ['nom', 'etudiants']


class ListeGroupeSoutenanceView(ListView):
    '''
    liste des enseignant
    '''
    model = GroupeSoutenance

    def get_context_data(self, **kwargs):
        context = super(ListeGroupeSoutenanceView, self).get_context_data(**kwargs)
        context["groupeSoutenances"] = GroupeSoutenance.objects.all()
        return context

class GroupeSoutenanceUpdate(UpdateView):
    '''
    Modification d'un enseignant
    '''
    model = GroupeSoutenance
    fields = ['nom', 'etudiants','evenement']
    template_name_suffix = '_update_form'

class GroupesSoutenancesDelete(DeleteView):
    model = GroupeSoutenance
    success_url = "/soutenances/groupeSoutenances"


class GroupeSoutenanceCreateView(CreateView):
    '''
     ajout d'un groupe
    '''
    model = GroupeSoutenance
    fields = ['nom', 'etudiants','evenement']
