from django.conf.urls import url
from .views import *
from . import views

app_name = 'Soutenances'
urlpatterns = [
    url(r'^$', views.login_form, name="login"),

    url(r'^api/groupes/$', GroupeList.as_view(), name='groupe-list'),
    url(r'^api/groupes/detail/(?P<pk>\d+)$', GroupeDestroy.as_view(), name='groupe-detail'),

    url(r'^api/etudiants/$', EtudiantList.as_view(), name='etudiant-list'),
    url(r'^api/etudiants/detail/(?P<pk>\d+)$', EtudiantDestroy.as_view(), name='etudiant-detail'),

    url(r'^api/enseignants/$', EnseignantList.as_view(), name='enseignant-list'),
    url(r'^api/enseignants/detail/(?P<pk>\d+)$', EnseignantDestroy.as_view(), name='enseignant-detail'),

    url(r'^api/evenements/$', EvenementList.as_view(), name='evenement-list'),
    url(r'^api/evenements/detail/(?P<pk>\d+)$', EvenementDestroy.as_view(), name='evenement-detail'),

    url(r'^api/users/$', UserList.as_view(), name='user-list'),
    url(r'^api/users/detail/(?P<pk>\d+)$', UserDestroy.as_view(), name='user-detail'),

    url(r'^api/groupesSoutenances/$', GroupeSoutenanceList.as_view(), name='groupesoutenances-list'),
    url(r'^api/groupesSoutenances/detail/(?P<pk>\d+)$', GroupeSoutenanceDestroy.as_view(), name='groupesoutenances-detail'),

    url(r'^api/slots/$', SlotsList.as_view(), name='slots-list'),
    url(r'^api/slots/detail/(?P<pk>\d+)$', SlotsDestroy.as_view(), name='slots-detail'),

    url(r'^api/Soutenances/$', SoutenanceList.as_view(), name='groupesoutenances-list'),
    url(r'^api/Soutenances/detail/(?P<pk>\d+)$', SoutenanceDestroy.as_view(), name='groupesoutenances-detail'),

    url(r'^registerchoice', views.register_choice, name="registerchoice"),
    url(r'^registerstudent', views.register_student, name="registerstudent"),
    url(r'^registerteacher', views.register_teacher, name="registerteacher"),
    url(r'^inscription_student', views.inscription_student, name="inscriptionstudent"),
    url(r'^inscription_teacher', views.inscription_teacher, name="inscriptionteacher"),
    url(r'^connexion', views.login_form, name="login"),
    url(r'^login', views.connexion, name="connexion"),
    url(r'^deconnexion', views.deconnexion, name="deconnexion"),
    url(r'^profil', views.profil, name="profil"),
    url(r'^updateprofil', views.updateprofil, name="updateprofil"),

    url(r'^mesSoutenances', views.mesSoutenances, name="mesSoutenances"),

    url(r'^groupes/delete/(?P<pk>\d+)$', views.GroupeDelete.as_view(),name="groupe_delete"),
    url(r'^groupes/$', ListeGroupeView.as_view(), name='groupe_list'),
    url(r'^groupes/add$', GroupeCreateView.as_view(), name="groupe_form"),
    url(r'^groupes/update/(?P<pk>\d+)$', GroupeUpdate.as_view(), name="groupe_update"),

    url(r'^etudiants/$', ListeEtudiantView.as_view(), name='etudiant_list'),
    url(r'^etudiants/add$', EtudiantCreateView.as_view(), name="etudiant_form"),
    url(r'^etudiants/update/(?P<pk>\d+)$',EtudiantUpdate.as_view(), name="etudiant_update"),
    url(r'^etudiants/delete/(?P<pk>\d+)$',EtudiantDelete.as_view(), name="etudiant_delete"),

    url(r'^enseignants/$', ListeEnseignantView.as_view(), name='enseignant_list'),
    url(r'^enseignants/add$', EnseignantCreateView.as_view(), name="enseignant_form"),
    url(r'^enseignants/update/(?P<pk>\d+)$',EnseignantUpdate.as_view(), name="enseignant_update"),
    url(r'^enseignants/delete/(?P<pk>\d+)$',EnseignantDelete.as_view(), name="enseignant_delete"),

    url(r'^evenements/$', ListeEvenementView.as_view(), name='evenement_list'),
    url(r'^evenements/add$', EvenementCreateView.as_view(), name='evenement_form'),
    url(r'^evenements/update/(?P<pk>\d+)$',EvenementUpdate.as_view(), name="evenement_update"),
    url(r'^evenements/delete/(?P<pk>\d+)$',EvenementDelete.as_view(), name="evenement_delete"),
    url(r'^evenements/detail/(?P<pk>\d+)$',EvenementDetail.as_view(), name="evenement_detail"),


    url(r'^groupeSoutenances/$', ListeGroupeSoutenanceView.as_view(), name='groupeSoutenances_list'),
    url(r'^groupeSoutenances/add$', GroupeSoutenanceCreateView.as_view(), name="groupeSoutenances_form"),
    url(r'^groupeSoutenances/update/(?P<pk>\d+)$',GroupeSoutenanceUpdate.as_view(), name="groupeSoutenances_update"),
    url(r'^groupeSoutenances/delete/(?P<pk>\d+)$',GroupesSoutenancesDelete.as_view(), name="groupeSoutenances_delete"),
]
