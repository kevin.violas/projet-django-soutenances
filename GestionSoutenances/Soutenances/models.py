from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from GestionSoutenances.settings import *

# Create your models here.

class UserManager(BaseUserManager):
    def create_user(self, pseudo, nom, prenom, password, groupe, classe):
         user = self.model(pseudo=pseudo)
         user.set_password(password)
         user.nom = nom
         user.prenom = prenom
         user.groupe = groupe
         user.classe = classe
         user.save(using=self._db)
         return user

class User(AbstractBaseUser):
    pseudo = models.CharField(max_length=20, default="eleve", unique=True)
    nom = models.CharField(max_length=20, default="")
    prenom = models.CharField(max_length=20, default="")
    groupe = models.CharField(max_length=20) # professeur ou etudiant
    classe = models.TextField(max_length=10, default="") # Groupe de l'étudiant
    USERNAME_FIELD = 'pseudo'
    REQUIRED_FIELDS = []
    objects = UserManager()

class Groupe(models.Model):
    nom = models.CharField(max_length=64)

    def get_absolute_url(self):
        return reverse("Soutenances:groupe_list")

    def __str__(self):
        return '{self.nom}'.format(self=self)

class Etudiant(models.Model):
    nom = models.CharField(max_length=64)
    prenom = models.CharField(max_length=64)
    photo = models.FileField(upload_to='etu/', max_length=2000)
    detail = models.CharField(max_length=200,null=True)
    groupe = models.ForeignKey(Groupe, on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def get_absolute_url(self):
        return reverse("Soutenances:etudiant_list")

    def __str__(self):
        return '{self.prenom} {self.nom}'.format(self=self)


class Enseignant(models.Model):
    nom = models.CharField(max_length=64)
    prenom = models.CharField(max_length=64)
    photo = models.ImageField(upload_to='ens/', max_length=2000)
    detail = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def get_absolute_url(self):
        return reverse("Soutenances:enseignant_list")

    def __str__(self):
        return '{self.prenom} {self.nom}'.format(self=self)




class Evenement(models.Model):
    nom = models.CharField(max_length=64)
    date_debut = models.CharField(max_length=64)
    date_fin = models.CharField(max_length=64)
    enseignant = models.ForeignKey(Enseignant, null=False)
    groupe = models.ForeignKey(Groupe, null=True)
    dureeSoutenances = models.IntegerField(null=True)

    def get_absolute_url(self):
        return reverse("Soutenances:evenement_list")

    def __str__(self):
        return '{self.nom}'.format(self=self)

class GroupeSoutenance(models.Model):
    nom = models.CharField(max_length=64,null=True)
    etudiants = models.ManyToManyField(Etudiant)
    evenement = models.ForeignKey(Evenement, on_delete=models.CASCADE, null=True)

    def get_absolute_url(self):
        return reverse("Soutenances:groupeSoutenances_list")

class Slots(models.Model):
    date = models.CharField(max_length=64)
    heure_debut = models.CharField(max_length=5)
    heure_fin = models.CharField(max_length=5)
    salle = models.CharField(max_length=64, null=True)
    evenement = models.ForeignKey(Evenement, on_delete=models.CASCADE, null=False)

class Soutenance(models.Model):
    slot = models.ForeignKey(Slots, on_delete=models.CASCADE, null=True)
    groupeSoutenance = models.ForeignKey(GroupeSoutenance, null=True)
